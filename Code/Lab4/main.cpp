#include<SFML/Graphics.hpp>
#include<time.h>
#include<iostream>
#include<stdlib.h>
#include<stdio.h>
#include <math.h>
#include "tPoint.h"

using namespace sf;
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	int num = 0;
	srand(time(0));
	RenderWindow window(sf::VideoMode(400, 400), "points");
	tPoint circ(300, 300);
	tLine line(80, 20, 30, 20);
	tryangle tryan(100, 100, 15);
	squad square(180, 240, 20);
	romb rom(120, 20, 20);
	tEllipse el(100, 250, 10, 1, 3);
	tCircle circl(200, 200, 10);
	RectangleShape lines(Vector2f(line.get_r(), 2));
	CircleShape tryang(tryan.get_r(), 3);
	CircleShape sq(square.get_r(), 4);
	CircleShape rb(rom.get_r(), 4);
	CircleShape shape(circl.get_r());
	CircleShape tshape(1);
	CircleShape oval(el.get_r());
	oval.setScale(el.get_xr(), el.get_yr());
	lines.rotate(line.get_rot());
	oval.setPosition(el.object_get_x(), el.object_get_y());
	window.draw(oval);
	tshape.setPosition(circ.object_get_x(), circ.object_get_y());
	window.draw(tshape);
	shape.setPosition(circl.object_get_x(), circl.object_get_y());
	window.draw(shape);
	lines.setPosition(line.object_get_x(), line.object_get_y());
	window.draw(lines);
	sq.setPosition(square.object_get_x(), square.object_get_y());
	sq.rotate(45.f);
	window.draw(sq);
	rb.setPosition(rom.object_get_x(), rom.object_get_y());
	window.draw(rb);
	tryang.setPosition(tryan.object_get_x(), tryan.object_get_y());
	window.draw(tryang);
	window.display();
	cout << "1) straight" << endl << "2) rotate around center" << endl;

	while (num != 1 && num != 2)
		cin >> num;
	if (num == 1) {
		circ.moveLin();
		circl.moveLin();
		line.moveLin();
		square.moveLin();
		rom.moveLin();
		tryan.moveLin();
		el.moveLin();
	}

	while (window.isOpen())
	{
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
		}

		window.clear();

		if (num == 1) {
			tshape.setPosition(circ.object_get_x(), circ.object_get_y());
			shape.setPosition(circl.object_get_x(), circl.object_get_y());
			lines.setPosition(line.object_get_x(), line.object_get_y());
			sq.setPosition(square.object_get_x(), square.object_get_y());
			rb.setPosition(rom.object_get_x(), rom.object_get_y());
			tryang.setPosition(tryan.object_get_x(), tryan.object_get_y());
			oval.setPosition(el.object_get_x(), el.object_get_y());
			circ.movePoint();
			circl.movePoint();
			square.movePoint();
			line.movePoint();
			rom.movePoint();
			tryan.movePoint();
			el.movePoint();

			if (circ.object_get_x() >= 400 || circ.object_get_x() <= 0) circ.xChange();

			if (circ.object_get_y() >= 400 || circ.object_get_y() <= 0) circ.yChange();

			if (circl.object_get_x() >= 400 || circl.object_get_x() <= 0) circl.xChange();

			if (circl.object_get_y() >= 400 || circl.object_get_y() <= 0) circl.yChange();

			if (square.object_get_x() >= 400 || square.object_get_x() <= 0) square.xChange();

			if (square.object_get_y() >= 400 || square.object_get_y() <= 0) square.yChange();

			if (line.object_get_x() >= 400 || line.object_get_x() <= 0) line.xChange();

			if (line.object_get_y() >= 400 || line.object_get_y() <= 0) line.yChange();

			if (rom.object_get_x() >= 400 || rom.object_get_x() <= 0) rom.xChange();

			if (rom.object_get_y() >= 400 || rom.object_get_y() <= 0) rom.yChange();

			if (tryan.object_get_x() >= 400 || tryan.object_get_x() <= 0) tryan.xChange();

			if (tryan.object_get_y() >= 400 || tryan.object_get_y() <= 0) tryan.yChange();

			if (el.object_get_x() >= 400 || el.object_get_x() <= 0) el.xChange();

			if (el.object_get_y() >= 400 || el.object_get_y() <= 0) el.yChange();

		}

		if (num == 2) {
			lines.rotate(0.05);
			shape.rotate(0.05);
			tshape.rotate(0.05);
			sq.rotate(0.05);
			rb.rotate(0.05);
			tryang.rotate(0.05);
			oval.rotate(0.05);
		}

		window.draw(tshape);
		window.draw(shape);
		window.draw(lines);
		window.draw(sq);
		window.draw(rb);
		window.draw(tryang);
		window.draw(oval);
		window.display();

	}

	return 0;

}
