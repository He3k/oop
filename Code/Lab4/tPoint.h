#pragma once
class tPoint {

protected:
	float x, y, xMove, yMove;
	int colP;

public:
	tPoint(int a, int b) {
		x = a;
		y = b;
	};

	void colSet()
	{
		colP = rand() % 6;
	}

	int object_get_x()
	{
		return x;
	}

	int object_get_y()
	{
		return y;
	}

	int object_get_colP()
	{
		return colP;
	}

	void moveLin()
	{
		int num1, num2;
		num1 = rand() % 2;
		num2 = rand() % 2;
		if (num1 == 1) {
			xMove = 0.05;
			yMove = 0;
		}

		else {
			xMove = 0;
			yMove = 0.05;
		}

		if (num2 == 1) {
			xMove *= -1;
			yMove *= -1;
		}
	}

	void xChange() {
		xMove *= -1;
	}

	void yChange() {
		yMove *= -1;
	}

	void movePoint() {
		x += xMove;
		y += yMove;
	}
};

class tCircle :public tPoint {
private:
	int r;

public:

	tCircle(int a, int b, int rx) : tPoint(a, b) {
		r = rx;
	};

	int get_r() {
		return r;
	}

	void moveLin() {
		tPoint::moveLin();
	}

	void movePoint() {
		tPoint::movePoint();
	}

	void xChange() {
		tPoint::xChange();
	}

	void yChange() {
		tPoint::yChange();
	}
};

class tEllipse :public tCircle {
private:
	int r;
	float xr, yr;

public:
	tEllipse(int a, int b, int r_x, int x_r, int y_r) : tCircle(a, b, r_x) {
		r = r_x;
		xr = x_r;
		yr = y_r;
	};

	int get_r() {
		return r;
	}

	int get_xr() {
		return xr;
	}

	int get_yr() {
		return yr;
	}

	void moveLin() {
		tCircle::moveLin();
	}

	void movePoint() {
		tCircle::movePoint();
	}

	void xChange() {
		tCircle::xChange();
	}

	void yChange() {
		tCircle::yChange();
	}
};

class tLine :public tPoint {

private:

	float r, rot;

public:

	tLine(int a, int b, int rx, int rotx) : tPoint(a, b) {
		r = rx;
		rot = rotx;
	};

	float get_r() {
		return r;
	}

	float get_rot() {
		return rot;
	}

	void moveLin() {
		tPoint::moveLin();
	}

	void movePoint() {
		tPoint::movePoint();
	}

	void xChange() {
		tPoint::xChange();
	}

	void yChange() {
		tPoint::yChange();
	}
};

class tryangle : public tPoint {

private:

	int r;

public:
	tryangle(int a, int b, int rx) : tPoint(a, b) {
		r = rx;
	};

	int get_r() {
		return r;
	}

	void moveLin() {
		tPoint::moveLin();
	}

	void movePoint() {
		tPoint::movePoint();
	}

	void xChange() {
		tPoint::xChange();
	}

	void yChange() {
		tPoint::yChange();
	}
};

class romb : public tPoint {

private:

	int r;

public:
	romb(int a, int b, int rx) : tPoint(a, b) {
		r = rx;
	};

	int get_r() {
		return r;
	}

	void moveLin() {
		tPoint::moveLin();
	}

	void movePoint() {
		tPoint::movePoint();
	}

	void xChange() {
		tPoint::xChange();
	}

	void yChange() {
		tPoint::yChange();
	}
};

class squad : public romb {

public:
	squad(int a, int b, int rx) : romb(a, b, rx) {
	};

	void moveLin() {
		romb::moveLin();
	}

	void movePoint() {
		romb::movePoint();
	}

	void xChange() {
		romb::xChange();
	}

	void yChange() {
		romb::yChange();
	}
};
