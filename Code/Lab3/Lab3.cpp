#include <./SFML/Graphics.hpp>
#include <ctime>
#include <cmath>
#include <iostream>
using namespace sf;
using namespace std;


class tPoint
{
private:
    int FrameRate = 120;
    int Quantity = 100, r = 10;
    float speed = 400 / FrameRate;
    int* Angle = new int[Quantity];
    float* Switch_x = new float[Quantity], * Switch_y = new float[Quantity];
    CircleShape* shape = new CircleShape[Quantity];

public:
    void LinMove() 
    {
        Clock clock;
        RenderWindow window(VideoMode(800, 600), "Random points");
        window.setFramerateLimit(FrameRate);
        srand(time(0));
        float time = clock.getElapsedTime().asMicroseconds();
        clock.restart();
        time = time / 800;
        for (int i = 0; i < Quantity; i++)
        {
            Angle[i] = rand() % 360;
            Switch_x[i] = cos(Angle[i] * 3.14 / 180) * speed;
            Switch_y[i] = sin(Angle[i] * 3.14 / 180) * speed;
            shape[i].setRadius(r);
            shape[i].setFillColor(Color(rand() % 255 + 1, rand() % 255 + 1, rand() % 255 + 1));
            shape[i].setPosition(rand() % (800 - r), rand() % (600 - r));
        }
        while (window.isOpen())
        {
            Event event;
            while (window.pollEvent(event))
            {
                if (event.type == Event::Closed)
                    window.close();
            }
            window.clear();
            for (int i = 0; i < Quantity; i++)
            {
                window.draw(shape[i]);
                float xi = shape[i].getPosition().x, yi = shape[i].getPosition().y;
                shape[i].move(Switch_x[i], Switch_y[i]);
                if ((((xi + 1) >= (800 - r - speed)) and (Switch_x[i] > 0)) or ((xi + 1) <= (0 + r)) and ((Switch_x[i] < 0)))
                {
                    Switch_x[i] = (-Switch_x[i]);
                }
                if ((((yi + 1) >= (600 - r - speed)) and (Switch_y[i] > 0)) or ((yi + 1) <= (0 + r)) and ((Switch_y[i] < 0)))
                {
                    Switch_y[i] = (-Switch_y[i]);
                }
            }
            window.display();
        }

    };

    void RandomMove() 
    {
        Clock clock;
        RenderWindow window(VideoMode(800, 600), "Random points");
        window.setFramerateLimit(FrameRate);
        srand(time(0));
        float time = clock.getElapsedTime().asMicroseconds();
        clock.restart();
        time = time / 800;
        for (int i = 0; i < Quantity; i++)
        {
            Angle[i] = rand() % 360;
            Switch_x[i] = cos(Angle[i] * 3.14 / 180) * speed;
            Switch_y[i] = sin(Angle[i] * 3.14 / 180) * speed;
            shape[i].setRadius(r);
            shape[i].setFillColor(Color(rand() % 255 + 1, rand() % 255 + 1, rand() % 255 + 1));
            shape[i].setPosition(rand() % (800 - r), rand() % (600 - r));
        }
        while (window.isOpen())
        {
            Event event;
            while (window.pollEvent(event))
            {
                if (event.type == Event::Closed)
                    window.close();
            }
            window.clear();
            for (int i = 0; i < Quantity; i++)
            {
                window.draw(shape[i]);
                Switch_x[i] = cos(Angle[i] * 3.14 / 180) * speed;
                Switch_y[i] = sin(Angle[i] * 3.14 / 180) * speed;
                float xi = shape[i].getPosition().x, yi = shape[i].getPosition().y;

                if ((xi + 1) >= (800 - r - speed))
                {
                    Angle[i] = rand() % 180 + 90;
                }
                else if ((yi + 1) >= (600 - r - speed))
                {
                    Angle[i] = rand() % 180 + 180;
                }
                else if ((yi + 1) <= (0 + r))
                {
                    Angle[i] = rand() % 180;

                }
                else if ((xi + 1) <= (0 + r))
                {
                    int l = rand() % 2;
                    if (l == 0)
                    {
                        Angle[i] = rand() % 90;
                    }
                    else
                    {
                        Angle[i] = rand() % 90 + 270;
                    }
                }
                else
                {
                    Angle[i] = rand() % 360;
                }
                shape[i].move(Switch_x[i], Switch_y[i]);
            }
            window.display();
        }

    } 

    ~tPoint()
    {
        delete[] Switch_x;
        delete[] Switch_y;
        delete[] Angle;
        delete[] shape;
    }
};

int main()
{
    setlocale(LC_ALL, "Russian");
    int num;
    cout << "1 - Lin move " << endl;
    cout << "2 - Rand move" << endl;
    cout << "Enter number : ";
    cin >> num;
    
    if (num == 1)
    {
        tPoint a;
        a.LinMove();
    }
    if (num == 2)
    {
        tPoint b;
        b.RandomMove();
    }
    if (num != 1 && num != 2) cout << "ERROR!";
    
    return 0;
}
