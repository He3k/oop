#include <iostream>
#include <random>
#include <math.h>

using namespace std;

int** genRandArray(int size, int maxValue) {
	int** arr = new int* [size + 2];
	for (int i = 0; i < size + 2; i++) {
		int size1 = rand() % 10;
		arr[i] = new int[size1];
		arr[i][0] = size1;
		for (int j = 1; j < size1 + 1; j++) {
			arr[i][j] = rand() % maxValue;
		}
	}
	return arr;
}

void print(int** arr, int size) {
	for (int i = 0; i < size + 2; i++) {
		cout << arr[i][0] << ": ";
		for (int j = 1; j < arr[i][0] + 1; j++) {
			cout << arr[i][j] << " ";
		}
		cout << endl;
	}
}

int main() {
	int stime = (int) time(NULL);
	srand(stime);
	int size = rand() % 10;
	int maxValue = 100;
	int** arr = genRandArray(size, maxValue);
	print(arr, size);
	delete(arr);
	return 0;
}
