#include <iostream>
#include <ctime>

using namespace std;

void NumA(int N, int** A, int* B) {
    int x = 0;
    for (int s = 0; s < N * N; ++s) {
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                if ((i + j) == s) B[x++] = A[i][(N - 1) - j];

            }
        }
    }
}

void NumB(int size, int** A, int* B) {
    int x = 0;
    for (int s = 0; s < size * size; ++s) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if ((i + j) == s) B[x++] = A[i][j];
            }
        }
    }
}

void NumC(int size, int** A, int* B) {
    int i = size / 2, j = size / 2, n = 0, x = 0;
    while (n != size) {
        n++;
        for (int k = 0; k < n; k++)
            B[x++] = A[i--][j];
        if (n == size) break;
        for (int k = 0; k < n; k++)
            B[x++] = A[i][j++];
        n++;
        for (int k = 0; k < n; k++)
            B[x++] = A[i++][j];
        for (int k = 0; k < n; k++)
            B[x++] = A[i][j--];
    }
}

void NumD(int n, int** A, int* B) {
    int i = 0, p = n / 2;
    for (int k = 1; k <= p; k++) { //Номер витка
        for (int j = k - 1; j < n - k + 1; ++j) B[i++] = A[k - 1][j]; //Верхний
        for (int j = k; j < n - k + 1; ++j) B[i++] = A[j][n - k]; //Правый
        for (int j = n - k - 1; j >= k - 1; --j) B[i++] = A[n - k][j];//Нижний
        for (int j = n - k - 1; j >= k; j--) B[i++] = A[j][k - 1];//Левый
    }
    if (n % 2 == 1) B[n * n - 1] = A[p][p];
}

int main() {
    srand(time(nullptr));
    int N = 5;
    int** A = new int* [N];
    int* B = new int[N * N];
    for (int i = 0; i < N; i++)
        A[i] = new int[N];

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            A[i][j] = rand() % 127;
        }
    }

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            cout << A[i][j] << "\t";
        }
        cout << endl;
    }

    NumA(N, A, B);
    cout << "A: ";
    for (int i = 0; i < N * N; ++i) {
        cout << B[i] << " ";
        B[i] = 0;
    }
    cout << endl;

    NumB(N, A, B);
    cout << "B: ";
    for (int i = 0; i < N * N; ++i) {
        cout << B[i] << " ";
    }
    cout << endl;

    NumC(N, A, B);
    cout << "C: ";
    for (int i = 0; i < N * N; ++i) {
        cout << B[i] << " ";
    }
    cout << endl;

    NumD(N, A, B);
    cout << "D: ";
    for (int i = 0; i < N * N; ++i) {
        cout << B[i] << " ";
    }

    for (int i = 0; i < N; i++) {
        delete[] A[i];
    }
    delete[] A;
    delete[] B;
    cout << endl;
}
