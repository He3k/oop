#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <SFML/Graphics.hpp>

using namespace sf;

using namespace std;

class Parent 
{
protected:
	int x, y;
	double slant;
	int red, green, blue;
	int x_right, x_left, y_right, y_left;
public: 
	virtual void CreatFigure(RenderWindow &window) = 0;
	virtual void Move() = 0;
};

class tPoint : public Parent 
{
protected:
	int rad;
public:
	tPoint() {
		rad = 3;
		x_right = 0;	x_left = 0;
		y_right = 0; 	y_left = 0;
		x = 20 + rand() % 850;
		y = 20 + rand() % 850;
		red = rand() % 255;
		green = rand() % 255;
		blue = rand() % 255;
		slant = rand() % 90;
	}
	void Move() {
		if (x_right == 0 && y_left == 0) {
			x++; y++;
			if (x == 885) {
				x_right = 885;
				y_right = 885;
			}
			if (y == 885) {
				x_right = 885;
			}
		}
		else if (x_right == 885 && y_right == 885) {
			x--; y++;
			if (y == 885) {
				y_right = 0;
				x_left = 0;
			}
		}
		else if (x_left == 0 && y_right == 0) {
			x--; y--;
			if (x == 0) {
				y_left = 0;
				x_left = 885;
			}
		}
		else if (x_left == 885 && y_left == 0) {
			x++; y--;
			if (y == 0) {
				x_right = 0;
				y_left = 0;
			}
		}
	}
	void CreatFigure(RenderWindow &window) {
		CircleShape point(rad);
		point.setFillColor(Color(red, green, blue));
		point.move(x, y);
		window.draw(point);
	}
};
class tCircle : public tPoint
{
public:

	tCircle(int radius = 20) {
		rad = radius;
	}
	tCircle(double radius) {
		rad = radius;
	}
	void CreatFigure(RenderWindow &window) override
	{
		CircleShape circle(rad);
		circle.setFillColor(Color(red, green, blue));
		circle.move(x, y);
		window.draw(circle);
	}
};
class tEllipse : public tCircle
{
protected:
public:
	void CreatFigure(RenderWindow &window) override
	{
		CircleShape ellipse(rad + 5);
		ellipse.setFillColor(Color(red, green, blue));
		ellipse.setScale(2, 1);
		ellipse.rotate(slant);
		slant += 0.5;
		ellipse.move(x, y);
		window.draw(ellipse);
	}
};
class tTriangle : public tCircle
{
protected:
	int angles;
public:
	tTriangle() {
		angles = 3;
	}
	void CreatFigure(RenderWindow &window) override
	{
		CircleShape traingle(rad, angles);
		traingle.setFillColor(Color(red, green, blue));
		traingle.rotate(slant);
		slant++;
		traingle.move(x, y);
		window.draw(traingle);
	}
};
class tRhomb : public tTriangle
{
public:
	tRhomb() {
		angles = 4;
		rad = 10 + rand() % 25;
	}
	void CreatFigure(RenderWindow &window) override
	{
		CircleShape rhomb(rad, angles);
		rhomb.setFillColor(Color(red, green, blue));
		rhomb.move(x, y);
		rhomb.setScale(2, 1);
		rhomb.rotate(slant);
		slant += 0.5;
		rhomb.rotate(slant);
		window.draw(rhomb);
	}
};
class tLine : public tPoint
{
protected:
	int width, height;
public:
	tLine() {
		width = 2;
		height = 30 + rand() % 50;
	}
	void CreatFigure(RenderWindow &window) {
		RectangleShape line(Vector2f(width, height));
		line.setFillColor(Color(red, green, blue));
		line.rotate(slant);
		slant++;
		line.move(x, y);
		window.draw(line);
	}
};
class tRectangle : public tLine
{
public:
	tRectangle() {
		width = 10 + rand() % 40;
		height = 10 + rand() % 70;
	}
	void CreatFigure(RenderWindow &window) override
	{
		RectangleShape rectangle(Vector2f(width, height));
		rectangle.setFillColor(Color(red, green, blue));
		rectangle.rotate(slant);
		slant++;
		rectangle.move(x, y);
		window.draw(rectangle);
	}
};
int main() {
	srand(time(NULL));

	int size = 140;
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	RenderWindow window(VideoMode(900, 900), "6 Lab", Style::Default, settings);

	vector<tPoint*>arrayFigure; ;
	
	for (int i = 0; i < size; i++) {
		if (i % 7 == 0) { arrayFigure.push_back(new tPoint()); }
		if (i % 7 == 1) { arrayFigure.push_back(new tCircle(30.5)); }
		if (i % 7 == 2) { arrayFigure.push_back(new tEllipse()); }
		if (i % 7 == 3) { arrayFigure.push_back(new tTriangle()); }
		if (i % 7 == 4) { arrayFigure.push_back(new tRhomb()); }
		if (i % 7 == 5) { arrayFigure.push_back(new tLine()); }
		if (i % 7 == 6) { arrayFigure.push_back(new tRectangle()); }
	}

	while (window.isOpen()) {
		window.clear(Color(255, 255, 255, 0));
		Event event;						  
		while (window.pollEvent(event)) {	 
			if (event.type == Event::Closed)  
				window.close();				  
		}
		for (int i = 0; i < size; i++) { 
			arrayFigure[i]->Move();
			arrayFigure[i]->CreatFigure(window);
		}
		window.setFramerateLimit(200);
		window.display();
	}
	return 0;
}
