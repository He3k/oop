#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <conio.h>
#include <iostream>
#include <unistd.h>


using namespace std;

class node {

	int data;
	class node* next;
	friend class list;
	friend class stack;
	friend class queue; 
};

int countt = 1;// статическая переменная, которая считает количество элементов

class list {
protected:
	node* head = NULL;// создание указателя

public:
	list(int d) {
		head = new node;
		head->data = d;
		head->next = NULL;
	}
	list() {
		head = new node;
		head->data = 0;
		head->next = NULL;
	}
	virtual void addList(int d) {
		try {
			if (head == NULL) throw "head = NULL";
			node* p = head;
			while (p->next != NULL) {
				p = p->next;
			}
			p->next = new node;
			p->next->data = d;
			p->next->next = NULL;
			countt++;
		}
		catch (char* msg) {// catch - программа выхватывает исключение
			cout << msg << endl;
		}
	}

	void printList() {
		try {
			if (head == NULL) throw "head = NULL"; 
			else {
				node* p = head;
				for (p = head; p != NULL; p = p->next) {
					cout << p->data << " ";
				}cout << endl;
			}
		}
		catch (char* msg) {
			cout << msg << endl;
		}
	}
	virtual void delElem() {
		countt--;
	}
};

class stack : public list { //создание стека 
private:
	node* tail = NULL;
public:
	stack(int d) :list(d) { //перегрузка конструктора
		tail = head;
	}
	stack() :list(0) {
		tail = head;
	}
	virtual void addList(int d) {
		try {
			if (head == NULL) throw "head = NULL";
			node* p = head;
			while (p->next != NULL) {
				p = p->next;
			}
			p->next = new node;
			p->next->data = d;
			tail = p->next;
			p->next->next = NULL;
			countt++;
		}
		catch (char* msg) {
			cout << msg << endl;
		}
	}
	virtual void delElem() {
		node* p = head;
		while (p->next != tail) {
			p = p->next;
		}
		tail = p;
		p->next = NULL;
		countt--;
	}
};

class queue : public list { //наследуем
public:
	queue(int d) :list(d) {
	}
	queue() :list(0) {
	}
	virtual void addList(int d) {
		node* p = head;
		while (p->next != NULL) {
			p = p->next;
		}
		p->next = new node;
		p->next->data = d;
		p->next->next = NULL;
		countt++;
	}
	virtual void delElem() {
		node* p = head;
		head = head->next;
		p = NULL;
		countt--;
	}
};



int main() {
	list* spis[2];
	spis[0] = new stack(1);
	spis[1] = new queue(10);
	for (int i = 1; i < 10; i++) {
		cout << "Стек:" << endl;
		spis[0]->printList();
		cout << "Количество элементов = " << countt << endl;
		sleep(5);
		system("clear");
		spis[0]->addList(i + 1);
	}
	cout << "Стек:" << endl;
	spis[0]->printList();
	cout << "Количество элементов = " << countt << endl;
	sleep(5);
	system("clear");
	for (int i = 1; i < 10; i++) {
		spis[0]->delElem();
		cout << "Стек:" << endl;
		spis[0]->printList();
		cout << "Количество элементов = " << countt << endl;
		sleep(5);
		system("clear");


	}
	for (int i = 10; i < 19; i++) {
		cout << "Очередь:" << endl;
		spis[1]->printList();
		cout << "Количество элементов = " << countt << endl;
		sleep(5);
		system("clear");
		spis[1]->addList(i + 1);
	}
	cout << "Очередь:" << endl;
	spis[1]->printList();
	cout << "Количество элементов = " << countt << endl;
	sleep(5);
	system("clear");
	for (int i = 1; i < 10; i++) {
		spis[1]->delElem();
		cout << "Очередь:" << endl;
		spis[1]->printList();
		cout << "Количество элементов = " << countt << endl;
		sleep(5);
		system("clear");
	}
	countt--;
	cout << "Количество элементов = " << countt << endl;
	system("PAUSE");
	return 0;
}
