#include <iostream>
#include <random>
#include <math.h>

using namespace std;

int* genRandArray(int size, int maxValue) {
	int* arr = new int[size + 2];
	arr[0] = size + 1;
	for (int i = 1; i < size + 2; i++) {
		arr[i] = rand() % maxValue;
	}
	return arr;
}

void print(int* arr) {
	cout << arr[0] << ": ";
	for (int i = 1; i < arr[0] + 1; i++) {
		cout << arr[i] << " ";
	}
	cout << endl;
}

int main() {
	int stime = (int) time(NULL);
	srand(stime);
	int size = rand() % 10;
	int maxValue = 100;
	int* arr = genRandArray(size, maxValue);
	print(arr);
	delete(arr);
	return 0;
}
