#include <iostream>
#include <cstdlib>
#include <ctime>
#include <SFML/Graphics.hpp>

using namespace sf;

using namespace std;

class Parent
{
protected:

	int x, y;
	int slant;
	int red, green, blue;
	int x_right, x_left, y_right, y_left;

public:
	virtual void Chaotic(RenderWindow &window) = 0;
	virtual void StraightRight(RenderWindow &window) = 0;
	virtual void StraightTop(RenderWindow &window) = 0;
	virtual void CreatFigure(RenderWindow &window) = 0;
};

class tPoint : public Parent
{
protected:

	int rad;

public:
	tPoint() {
		rad = 3;
		x_right = 0; x_left = 0;
		y_right = 0; y_left = 0;
		x = 20 + rand() % 850;
		y = 20 + rand() % 850;
		red = rand() % 255;
		green = rand() % 255;
		blue = rand() % 255;
		slant = rand() % 90;
	}

	void Chaotic(RenderWindow &window) {
		if (x_right == 0 && y_left == 0) {
			x++; y++;
			if (x == 885) {
				x_right = 885;
				y_right = 885;
			}

			if (y == 885) {
				x_right = 885;
			}
		}

		else if (x_right == 885 && y_right == 885) {
			x--; y++;
			if (y == 885) {
				y_right = 0;
				x_left = 0;
			}
		}

		else if (x_left == 0 && y_right == 0) {
			x--; y--;
			if (x == 0) {
				y_left = 0;
				x_left = 885;
			}
		}

		else if (x_left == 885 && y_left == 0) {
			x++; y--;
			if (y == 0) {
				x_right = 0;
				y_left = 0;
			}
		}
	}

	void StraightRight(RenderWindow &window) {
		if (x_right == 0) {
			x++;
			if (x == 885) {
				x_right = 885;
				x_left = 885;
			}
		}

		if (x_left == 885) {
			x--;
			if (x == 0) {
				x_right = 0;
				x_left = 0;
			}
		}
	}

	void StraightTop(RenderWindow &window) {
		if (y_right == 0) {
			y++;
			if (y == 885) {
				y_right = 885;
				y_left = 885;
			}
		}

		if (y_left == 885) {
			y--;
			if (y == 0) {
				y_right = 0;
				y_left = 0;
			}
		}
	}

	void CreatFigure(RenderWindow &window) {
		CircleShape point(rad);
		point.setFillColor(Color(red, green, blue));
		point.move(x, y);
		window.draw(point);
	}
};

class tCircle : public tPoint//круг

{

public:

	tCircle() {
		rad = 10 + rand() % 20;
	}

	tCircle(double radius) {
		rad = radius;
	}

	void CreatFigure(RenderWindow &window) override
	{
		CircleShape circle(rad);
		circle.setFillColor(Color(red, green, blue));
		circle.move(x, y);
		window.draw(circle);

	}
};

class tEllipse : public tCircle
{
protected:

public:

	void CreatFigure(RenderWindow &window) override
	{
		CircleShape ellipse(rad + 5);
		ellipse.setFillColor(Color(red, green, blue));
		ellipse.setScale(2, 1);
		ellipse.rotate(slant);
		slant++;
		ellipse.move(x, y);
		window.draw(ellipse);
	}
};

class tTraingle : public tEllipse
{
protected:
	int angles;

public:

	tTraingle() : angles(3) {}
	void CreatFigure(RenderWindow &window) override
	{
		CircleShape traingle(rad, angles);
		traingle.setFillColor(Color(red, green, blue));
		traingle.rotate(slant);
		slant++;
		traingle.move(x, y);
		window.draw(traingle);
	}
};

class tRhomb : public tTraingle
{
public:

	tRhomb() {
		angles = 4;
	}

	void CreatFigure(RenderWindow &window) override
	{
		CircleShape rhomb(rad, angles);
		rhomb.setFillColor(Color(red, green, blue));
		rhomb.move(x, y);
		rhomb.setScale(2, 1);
		rhomb.rotate(slant);
		window.draw(rhomb);
	}
};

class tLine : public tPoint
{
protected:
	int width, height;
public:

	tLine() {
		width = 2;
		height = 30 + rand() % 50;
	}

	virtual void CreatFigure(RenderWindow &window) {
		RectangleShape line(Vector2f(width, height));
		line.setFillColor(Color(red, green, blue));
		line.rotate(slant);
		slant++;
		line.move(x, y);
		window.draw(line);
	}
};

class tRectangle : public tLine
{
public:

	tRectangle() {
		width = 10 + rand() % 40;
		height = 10 + rand() % 70;
	}

	void CreatFigure(RenderWindow &window) override
	{
		RectangleShape rectangle(Vector2f(width, height));
		rectangle.setFillColor(Color(red, green, blue));
		rectangle.rotate(slant);
		slant++;
		rectangle.move(x, y);
		window.draw(rectangle);
	}
};

int main() {
	srand(time(NULL));
	const int size = 30;
	tPoint arrPoint[size];
	tCircle arrCircle[size] = { tCircle(50.5), tCircle(50.1), tCircle(50.3) };
	tEllipse arrEllipse[size];
	tTraingle arrTraingle[size];
	tRhomb arrRhomb[size];
	tLine arrLine[size];
	tRectangle arrRectangle[size];
	ContextSettings settings;
	settings.antialiasingLevel = 8;
	RenderWindow window(VideoMode(900, 900), "5 Lab", Style::Default, settings);//окно 

	while (window.isOpen()) {
		window.clear(Color(255, 255, 255, 0));
		Event event; 
		while (window.pollEvent(event)) { 
			if (event.type == Event::Closed) 
				window.close();
		}

		for (int i = 0; i < size; i++) {

			if (i % 3 == 0) {
				arrPoint[i].StraightRight(window);
				arrPoint[i].CreatFigure(window);
				arrCircle[i].StraightRight(window);
				arrCircle[i].CreatFigure(window);
				arrEllipse[i].StraightRight(window);
				arrEllipse[i].CreatFigure(window);
				arrTraingle[i].StraightRight(window);
				arrTraingle[i].CreatFigure(window);
				arrRhomb[i].StraightRight(window);
				arrRhomb[i].CreatFigure(window);
				arrLine[i].StraightRight(window);
				arrLine[i].CreatFigure(window);
				arrRectangle[i].StraightRight(window);
				arrRectangle[i].CreatFigure(window);
			}

			else if (i % 3 == 1) {
				arrPoint[i].StraightTop(window);
				arrPoint[i].CreatFigure(window);
				arrCircle[i].StraightTop(window);
				arrCircle[i].CreatFigure(window);
				arrEllipse[i].StraightTop(window);
				arrEllipse[i].CreatFigure(window);
				arrTraingle[i].StraightTop(window);
				arrTraingle[i].CreatFigure(window);
				arrRhomb[i].StraightTop(window);
				arrRhomb[i].CreatFigure(window);
				arrLine[i].StraightTop(window);
				arrLine[i].CreatFigure(window);
				arrRectangle[i].StraightRight(window);
				arrRectangle[i].CreatFigure(window);
			}

			else {
				arrPoint[i].Chaotic(window);
				arrPoint[i].CreatFigure(window);
				arrCircle[i].Chaotic(window);
				arrCircle[i].CreatFigure(window);
				arrCircle[i].Chaotic(window);
				arrCircle[i].CreatFigure(window);
				arrEllipse[i].Chaotic(window);
				arrEllipse[i].CreatFigure(window);
				arrTraingle[i].Chaotic(window);
				arrTraingle[i].CreatFigure(window);
				arrRhomb[i].Chaotic(window);
				arrRhomb[i].CreatFigure(window);
				arrLine[i].Chaotic(window);
				arrLine[i].CreatFigure(window);
				arrRectangle[i].Chaotic(window);
				arrRectangle[i].CreatFigure(window);
			}
		}
		window.setFramerateLimit(200);
		window.display();
	}

	return 0;
}
